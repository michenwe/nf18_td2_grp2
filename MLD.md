<h1>MLD</h1>

<h3><b>Animal</b>(#id_animal:int, nom:string, espèce:type_animal, date_naiss:Date, num_puce:int, num_pass:int, #espece=>Espece) AVEC nom NOT NULL, prenom NOT NULL, PROJECTION(Animal, espece) = PROJECTION(Espece, nom)</h3><br/>
<br/>

<h3><b>type_animal</b>(#type : {félin, oiseau, canidé, reptile, rongeur, autre})</h3><br/>
<br/>

<h3><b>Client</b>(#id_client:int, nom:string, prénom:string, date_naiss:Date, code_postal:int, ville:string, rue:string, num_tel:string) AVEC nom NOT NULL, prenom NOT NULL, date_naiss NOT NULL, code_postal NOT NULL, ville NOT NULL, rue NOT NULL, num_tel NOT NULL, INTERSECTION (PROJECTION(client, id_client), PROJECTION(assistant, id_assis), PROJECTION(veterinaire, id_vete)) = {}</h3><br/>
<br/>

<h3><b>Proprietaires</b>(date_debut:Date, date_fin:Date, #animal=>Animal, #client=>Client) AVEC date_debut NOT NULL, PROJECTION(Proprietaires, animal) = PROJECTION(Animal, id_animal), PROJECTION(Proprietaires, client) = PROJECTION(Client, id_client)</h3><br/>
<br/>

<h3><b>Assistant</b>(#id_assis:int, nom:string, prénom:string, date_naiss:Date, code_postal:int, ville:string, rue:string, num_tel:string, specialite:type_animal)</h3><br/>
<br/>

<h3><b>Veterinaire</b>(#id_vete:int, nom:string, prénom:string, date_naiss:Date, code_postal:int, ville:string, rue:string, num_tel:string, specialite:type_animal)</h3><br/>
<br/>

<h3><b>Veterinaire_responsable</b>(date_debut:Date, date_fin:Date, #animal=>Animal, #veterinaire=>Veterinaire) AVEC date_debut NOT NULL, PROJECTION(Veterinaire_responsable, animal) = PROJECTION(Animal, id_animal), PROJECTION(Veterinaire_responsable, veterinaire) = PROJECTION(Veterinaire, id_vete)</h3><br/>
<br/>

<h3><b>Consultation</b>(date:Date, observation:string, procedure:string, taille:double, poids:double, #assistant=>Assistant, #veterinaire=>Veterinaire) AVEC date NOT NULL</h3><br/>
<br/>

<h3><b>Traitement</b>(#id_traite:int, debut_traitement:Date, duree:Date, #consultation=>Consultation, #dossier=>Dossier_Medical) AVEC debut_traitement NOT NULL, duree NOT NULL</h3><br/>
<br/>

<h3><b>Analyse</b>(date:Date, #lien:string, #assistant=>Assistant, #veterinaire=>Veterinaire, #dossier=>Dossier_Medical)</h3><br/>
<br/>

<h3><b>Medicament</b>(#nom_molecule:string, description:string) AVEC description NOT NULL</h3><br/>
<br/>

<h3><b>Prescription</b>(quantite:int, #traitement=>Traitement, #medicament=>Medicament) AVEC PROJECTION(Prescription, traitement) = PROJECTION(Traitement, id_traite), PROJECTION(Prescription, medicament) = PROJECTION(Medicament, nom_molecule)</h3><br/>
<br/>

<h3><b>Espece</b>(#nom:type_animal, #assistant=>Assistant, #veterinaire=>Veterinaire)</h3><br/>
<br/>

<h3><b>Autorisation</b>(#medicament=>Medicament, #espece=>Espece)</h3><br/>
<br/>

<li>#consultation key?</li>
