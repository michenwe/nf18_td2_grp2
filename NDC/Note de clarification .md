<h1>NDC</h1>

<ul>

<li><h2><i>Identité</i>: id(clé), nom, prénom, date_naiss, code_postal, ville, rue, num_tel</h2></li>
<ul> 
<li>est une classe abstraite</li>
<li>composé par 2 classes filles: <ins><b>Client</b></ins> et <ins><b>Personnel</b></ins></li>
</ul>
<br/>

<li><h2>Client</h2></li>
<ul> 
<li>est une classe fille d'<ins><b>Identité</b></ins></li>
<li>est propriétaire de plusieurs <ins><b>Animal</b></ins> (1..n-*)</li>
<li>ne peut pas être <ins><b>Personnel</b></ins> (héritage exclusif)</li>
</ul>
<br/>

<li><h2>Personnel: spécialité </h2></li>
<ul> 
<li>est de type <strong>type_animal</strong></li>
<li>est une classe fille d'<ins><b>Identité</b></ins></li>
<li>composé par 2 classes filles: <ins><b>Assistant</b></ins> et <ins><b>Vétérinaire</b></ins></li>
<li>a pour spécialité des <ins><b>Espèce</b></ins>(*-*)</li>
<li>fait les <b>Consultation</b> sur les <ins><b>Animal</b></ins>  (*-*)</li>
<ul><li>avec date, observation, procédure, taille, poids</li></ul>
<li>fait les <b>Analyse</b> sur les <ins><b>Animal</b></ins> (*-*)</li>
<ul><li>avec date, lien</li></ul>
<li>ne peut pas être <ins><b>Client</b></ins> (héritage exclusif)</li>
</ul>
<br/>

<li><h2>Assistant</h2></li>
<ul> 
<li>est une classe fille de <ins><b>Personnel</b></ins></li>
<li>ne peut pas être <ins><b>Vétérinaire</b></ins> (héritage exclusif)</li>
</ul>
<br/>

<li><h2>Vétérinaire</h2></li>
<ul> 
<li>est une classe fille de <ins><b>Personnel</b></ins></li>
<li>ne peut pas être <ins><b>Assistant</b></ins> (héritage exclusif)</li>
</ul>
<br/>

<li><h2>Animal: id(clé), nom, espèce, date_naiss, num_puce, num_pass</h2></li>
<ul> 
<li>est d'un <ins><b>Espèce</b></ins> (*-1)</li>
<li>a des <ins><b>Client</b></ins> comme propriétaire (1..n-*)</li>
<ul><li>avec une date de début et une date de fin</li></ul>
<li>est traité par les <ins><b>Vétérinaire </b></ins> (*-1..n)</li>
<ul><li>avec une date de début et une date de fin</li></ul>
</ul>
<br/>

<li><h2>Espèce: nom(clé)</h2></li>
<ul>
<li>est de type <b>type_animal</b></li>
</ul>
<br/>

<li><h2>Traitement: id(clé), début_traitement, durée </h2></li>
<ul>
<li>prescrit par un <ins><b>Vétérinaire </b></ins>(*-1)</li> 
<li>concerne à un <ins><b>Animal </b></ins>(*-1)</li> 
<li>est composé de plusieurs <ins><b>Médicament</b></ins>(*-1..n)</li> 
<ul><li>contient une prescription indiquant la quantité de chaque médicament</li></ul>        
</ul>
<br/>

<li><h2>Médicament: nom_molécule(clé), description </h2></li>
<ul>
<li>est autorisé pour certaines <ins><b>Espèce</b></ins>(*-*)</li> 
</ul>
<br/>

</ul>

<h2><strong>type_animal</strong></h2>
<ul> 
<li>félins, canidés, reptiles, rongeurs, oiseaux, autres</li>
</ul>
<br/>

<h1>Explications</h1>
<ul>
<li>
<h2>Héritage exclusif</h2>
<ul>
<li>Un Client ne peut pas être un Personnel</li>
<li>Un Personnel ne peut pas être un Client</li> 
<li>Un Assistant ne peut pas être un Vétérinaire</li>
<li>Un Vétérinaire ne peut pas être un Assistant</li> 
</ul>
</li>
<br/>
<li><h2>Le choix de ne pas mettre la classe dossier médical</h2>
<ul> 
<li>C'est pas utile d'avoir les associations 1..1 (entre Analyse et Dossier_Medical, Traitement et Dossier_Medical, Consultation et Dossier_Medical)</li> 
<li>Du coup on a remplacé les classes Analyse et Consultation par les classes d'association entre Animal et Personnel (identifié par date id_personnel et id_animal) et permit seulement le Vétérinaire prescrire un Traitement</li>
</ul></li>

</ul>